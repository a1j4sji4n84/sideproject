﻿using System;
using System.Threading;


class RunThread
{

    public void Run() {
        Thread t1 = new Thread(new ParameterizedThreadStart(FuncThreadTest));
        t1.Start("執行序1");
        Thread t2 = new Thread(new ParameterizedThreadStart(FuncThreadTest));
        t2.Start("執行序2");
    }

    static Singleton SingletonTmp = null;
    public static void FuncThreadTest(object __ThreadName)
    {
        Singleton objSingleton = Singleton.GetInstance();
        if (SingletonTmp == null)
        {
            SingletonTmp = objSingleton;
        }
        else if (SingletonTmp == objSingleton)
        {
            Console.WriteLine("同一個Instance");
        }
        else { 
            Console.WriteLine("不同個Instance");
        }
        //抽號碼牌
        //for (int i = 1; i <= 100000; i++)
        //{
        //    Console.WriteLine(__ThreadName + ":" + objSingleton.GetNumber().ToString());
        //}
    }
}
    

