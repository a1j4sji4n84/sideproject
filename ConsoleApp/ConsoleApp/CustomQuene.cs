﻿
using System;
using System.Collections;
using System.Collections.Generic;

class CustomQuene
{
    public void Run() {
        //先進先出原則

        //可選擇是否設定型別
        Queue<string> myQString = new Queue<string>(1); 
        Queue myQ = new Queue();

        int[] Arr = new int[5];

        //Enqueue：放入
        myQString.Enqueue("str");
        myQString.Enqueue("str2");
        
        //數量
        Console.WriteLine("Count:{0}", myQString.Count);

        myQ.Enqueue("str");
        myQ.Clear();
        myQ.Enqueue(1);
        myQ.Enqueue(2);
        myQ.Enqueue(3);
        myQ.CopyTo(Arr, 2); //Array的位置1開始放入整個Queue，故需ArraySize >= 位置 + QueueSize
        foreach (int num in Arr) { 
            Console.WriteLine("Arr:" + num.ToString());
        }

        //Dequeue：取出
        Console.WriteLine("myQ:" + myQ.Dequeue().ToString());

        //Peek：取得值不取出
        Console.WriteLine("myQ:" + myQ.Peek().ToString());

        

        
    }
}

