﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

public class Utilities
{
    class InnerClass
    {
        internal static readonly Utilities instance;
        static InnerClass()
        {
            if (instance == null)
            {
                instance = new Utilities();
            } 
        }
    }

    public static Utilities GetInstance() {
        return InnerClass.instance;
    }

   

    /// <summary>
    /// 紀錄LOG
    /// 路徑：TIAHR_LOG\\WebLog\\作業名稱\\日期\\檔名.txt
    /// 內容：[Key]日期：內容
    /// </summary>
    /// <param name="__FileClass">資料夾分類名稱(作業名稱)</param>
    /// <param name="__FileName">檔名</param>
    /// <param name="__LogKey"></param>
    /// <param name="__LogContent">內容</param>
    /// <param name="__User">登入人員姓名，Ajax和IEFormCustomEvent無法使用Session</param>
    public void WriteLog(String __FileClass, String __FileName, String __LogKey, String __LogContent, String __User)
    {
        try
        {

            String sFilePath = System.Configuration.ConfigurationManager.AppSettings["CustomLogFilePath"]; //取得目前頁面路徑
            if (sFilePath != null)
            {
                //再次寫入失敗，則改寫入ErrorLog
                if (System.Text.RegularExpressions.Regex.Split(__LogContent, "資源占用").Length > 2 && __FileClass.IndexOf("ErrorLog") == -1)
                {
                    WriteLog("ErrorLog_" + __FileClass, __FileName, __LogKey, __LogContent, __User);
                    return;
                }
                //寫入ErrorLog失敗，則不寫入
                else if (System.Text.RegularExpressions.Regex.Split(__LogContent, "資源占用").Length > 3 && __FileClass.IndexOf("ErrorLog") != -1)
                {
                    return;
                }
                String sFileName = __FileName + ".txt";
                DateTime NowTime = DateTime.Now;
                String sDate = NowTime.ToString("yyyyMMdd");
                String sDateTime = NowTime.ToString("yyyy-MM-dd HH:mm:ss");
                sFilePath = sFilePath + "\\WebLog\\" + __FileClass + "\\" + sDate;
                if (!System.IO.Directory.Exists(sFilePath))
                {
                    System.IO.Directory.CreateDirectory(sFilePath);
                }
                System.IO.File.AppendAllText(sFilePath + "\\" + sFileName, Environment.NewLine + "[" + __User + "][" + __LogKey + "]" + sDateTime + "：" + __LogContent);
            }
        }
        catch (Exception ex)
        {
            WriteLog(__FileClass, __FileName, __LogKey, "資源占用再次寫入！" + ex.Message.ToString() + __LogContent, __User);
        }
    }
}

