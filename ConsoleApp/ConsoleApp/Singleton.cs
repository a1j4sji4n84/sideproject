﻿using System;
using System.Collections.Generic;
using System.Text;

//https://ithelp.ithome.com.tw/articles/10196899
//https://ithelp.ithome.com.tw/articles/10237991

class Singleton
{
    private static Singleton instance = null;
    private static object objlock = new object();
    public static Singleton GetInstance()
    {
        if (instance == null)
        {
            //lock (objlock)
            //{
                if (instance == null)
                {
                    instance = new Singleton();
                }
            //}
        }
        return instance;
    }

    //Static Inner Class
    //class InnerClass
    //{
    //    internal static readonly Singleton instance;
    //    static InnerClass()
    //    {
    //        if (instance == null)
    //        {
    //            instance = new Singleton();
    //        }
    //    }
    //}
    //public static Singleton GetInstance()
    //{
    //    return InnerClass.instance;
    //}

    //抽號碼牌
    private static readonly object numberBlock = new object();
    protected int Counter = 0;
    public int GetNumber()
    {
        lock (numberBlock)
        {
            Counter++;
            return Counter;
        }

    }
}

